#순서유지 중복제거
a = [1,2,4,5,3,4,6,7,7,8,6,8,3]
my_set = set()
res = []
for e in a:
    if e not in my_set:
        res.append(e)
        my_set.add(e)
print(res)

#함수형
def OrderedSet(list):
    my_set = set()
    res = []
    for e in list:
        if e not in my_set:
            res.append(e)
            my_set.add(e)

    return res

os=OrderedSet([10,5,3,10,2,4,9,5,53,23,2,3,4,10,24,35,23])
print(os)